import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { ProductComponent } from './product/product.component';
import { NgModel } from '@angular/forms';
import { NgModule } from '@angular/core';


const routes: Routes = [
  { 
    path: '', component: ProductComponent
  },
  {
    path: 'details/:id/:name', component: DetailComponent
  }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductRoutingModule {}