import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { ProductComponent } from './product/product.component';
import { DetailComponent } from './detail/detail.component';
import { ProductRoutingModule } from './product-routing.module';
import { RouterModule } from '@angular/router';
import { ProductService } from './product.service';




@NgModule({
  declarations: [
    CardComponent,
    ProductComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    IvyCarouselModule,
    RouterModule
  ],
  exports: [
    CardComponent,
    DetailComponent,
    ProductComponent,
    ProductRoutingModule
  ]
})
export class ProductModule { }
