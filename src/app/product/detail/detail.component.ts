import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.sass']
})
export class DetailComponent implements OnInit {

  showSection: string = 'overview';
  product:any;
  selectedSize: any;
  selectedIndex: number = 0;
  selectedImage:any;
  skus:any;

  constructor(private route: ActivatedRoute, private productService: ProductService) {
    this.skus = this.productService.skus;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      debugger;
      this.product = this.productService.getProduct(params['id']);
      this.selectedSize = this.product.sizes[0];
      this.selectImage(0);
      console.log(params, this.product);
    })
  }

  selectSize(index:number): void {
    this.selectedSize = this.product.sizes[index];
    this.selectImage(0);
  }

  selectImage(index: number) :void{
    this.selectedImage = this.selectedSize.images[index];
  }
 
}
