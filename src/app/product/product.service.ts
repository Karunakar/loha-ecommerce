import { Injectable } from '@angular/core';
import * as produtsJson from '../../assets/products.json';
import * as skuData from '../../assets/sku.json';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: any;
  skus:any;

  constructor() {
    const products :any = produtsJson;
    const skus: any = skuData;
    this.products =  products.default || [];
    this.skus = skus.default;
  }

  getProducts(): any{
    return this.products;
  }

  getProduct(id:any) :any {
    let item = this.products.filter( (item:any) =>  item.id == id);
    return item[0] || {};
  }

}
