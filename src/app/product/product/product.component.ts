import { Component } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent {

  products: any;
  skus:any;

  constructor(private productService: ProductService) {
    this.products =this.productService.getProducts();
    this.skus = this.productService.skus;
    console.log(this.products);
  }

}
