import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './common/contact/contact.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product/product.component';
import { DetailComponent } from './product/detail/detail.component';

const routes: Routes = [
  {
    path: 'contact', component: ContactComponent
  },
  {
    path: '', component: HomeComponent
  }, 
  {
    path: 'products', loadChildren: () => import ('./product/product.module').then(m => m.ProductModule)
  },
  /*{
    path: 'product', component: ProductComponent,
    children: [
      {
        path: 'list', component: ProductComponent
      },
      {
        path: 'details', component: DetailComponent
      }
    ]
  } */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
