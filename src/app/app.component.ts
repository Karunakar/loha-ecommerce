import { Component } from '@angular/core';
import { ProductService } from './product/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {


  constructor(private productService: ProductService) {}

 

  title:any = 'ecommerce';
    dots:boolean =  true;
    pauseOnHover:boolean =  true;
    autoplay:boolean =  true;
    autoplayInterval:number =  4000;
    arrows: boolean =  true;
    cellsToScroll:number =  1;
    cellsToShow:number = 1

   
}
