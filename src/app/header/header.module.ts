import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CommonCompModule } from '../common/common.module';
import { HeaderRoutingModule } from './header/header-routing.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    CommonCompModule,
    RouterModule
  ],
  exports: [HeaderComponent, FooterComponent, HeaderRoutingModule]
})
export class HeaderModule { }
