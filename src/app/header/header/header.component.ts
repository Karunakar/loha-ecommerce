import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  navigation:string = '';


  constructor(private route: ActivatedRoute, private router: Router) {
    console.log(route.pathFromRoot)
  }

  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if(event instanceof NavigationEnd) {
        this.navigation = event.url;
        console.log(event);
      }
    })
  }

}
