import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProductComponent } from 'src/app/product/product/product.component';
import { ContactComponent } from 'src/app/common/contact/contact.component';
import { AboutComponent } from 'src/app/common/about/about.component';
import { FaqComponent } from 'src/app/common/faq/faq.component';


const routes: Routes = [
  { 
    path: 'products', component: ProductComponent
  },
  {
    path: 'contact', component: ContactComponent
  },
  {
    path: 'about', component: AboutComponent
  },
  {
    path: 'faq', component: FaqComponent
  }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HeaderRoutingModule {}