import { Component } from '@angular/core';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent {

  products: any;
  skus:any;

  constructor(private productService: ProductService) {
    this.products = Object.assign([], this.productService.getProducts());
    this.products = this.products.splice(0,4);
    this.skus = this.productService.skus;
  }


  images = [
    "assets/images/EF5A7067a.jpg",
    "assets/images/EF5A7072.jpg",
    "assets/images/EF5A7209.jpg",
    "https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-01.jpg", 
    "https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-02.jpg",
    "https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-03.jpg",
    "https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-04.jpg"

    ];


    tempJson = {
      "decisions": [
        {
          "sys_id": "29b1c8b0074321102722f66c7c1ed0fe",
          "label": "Would like to test something ?",
          "options": "60f18cb0074321102722f66c7c1ed058,6fd18cb0074321102722f66c7c1ed031",
          "result": null
        },
        {
          "sys_id": "2d534434074321102722f66c7c1ed05c",
          "label": "Do you want to go around and see what is available for you.",
          "options": null,
          "result": "a83820b58f723300e7e16c7827bdeed2"
        },
        {
          "sys_id": "60f18cb0074321102722f66c7c1ed058",
          "label": "No, I am good with what exists",
          "options": "ec13c4b0074321102722f66c7c1ed017,2d534434074321102722f66c7c1ed05c",
          "result": null
        },
        {
          "sys_id": "6fd18cb0074321102722f66c7c1ed031",
          "label": "Yes , i would like to change the title",
          "options": "e14200f0074321102722f66c7c1ed089",
          "result": null
        },
        {
          "sys_id": "e14200f0074321102722f66c7c1ed089",
          "label": "What you want to change now ?",
          "options": null,
          "result": "1c832706732023002728660c4cf6a7b9"
        },
        {
          "sys_id": "ec13c4b0074321102722f66c7c1ed017",
          "label": "What would like to do you know ?",
          "options": null,
          "result": "e329de99731423002728660c4cf6a73c"
        }
      ],
      "data": {
        "29b1c8b0074321102722f66c7c1ed0fe": {
          "sys_id": "29b1c8b0074321102722f66c7c1ed0fe",
          "label": "Would like to test something ?",
          "options": "60f18cb0074321102722f66c7c1ed058,6fd18cb0074321102722f66c7c1ed031",
          "result": null
        },
        "2d534434074321102722f66c7c1ed05c": {
          "sys_id": "2d534434074321102722f66c7c1ed05c",
          "label": "Do you want to go around and see what is available for you.",
          "options": null,
          "result": "a83820b58f723300e7e16c7827bdeed2"
        },
        "60f18cb0074321102722f66c7c1ed058": {
          "sys_id": "60f18cb0074321102722f66c7c1ed058",
          "label": "No, I am good with what exists",
          "options": "ec13c4b0074321102722f66c7c1ed017,2d534434074321102722f66c7c1ed05c",
          "result": null
        },
        "6fd18cb0074321102722f66c7c1ed031": {
          "sys_id": "6fd18cb0074321102722f66c7c1ed031",
          "label": "Yes , i would like to change the title",
          "options": "e14200f0074321102722f66c7c1ed089",
          "result": null
        },
        "e14200f0074321102722f66c7c1ed089": {
          "sys_id": "e14200f0074321102722f66c7c1ed089",
          "label": "What you want to change now ?",
          "options": null,
          "result": "1c832706732023002728660c4cf6a7b9"
        },
        "ec13c4b0074321102722f66c7c1ed017": {
          "sys_id": "ec13c4b0074321102722f66c7c1ed017",
          "label": "What would like to do you know ?",
          "options": null,
          "result": "e329de99731423002728660c4cf6a73c"
        }
      }
    };

  carouselPoperties ={
    dots: true,
    pauseOnHover:true,
    autoplay:  true,
    autoplayInterval: 2000,
    arrows: true,
    cellsToScroll :1,
    cellsToShow: 1,
    loop: false,
    cellWidth: 100
  }

  question:any ={
    question: '',
    childs : ''
  }

  showPrevious:boolean = true;


  getChilds(childs:string) : any {
    console.log(childs);
    const list:any = this.tempJson.data;
    let childenrs:any = [];
    childs.split(",").forEach(option => {
      console.log()
      childenrs.push(list[option]);
    });
   // console.log(childenrs);
    return childenrs;
  }

  previousQuestion(currentQuestion:string) {
    debugger
    this.tempJson.decisions.forEach(element => {
       let options:any = element.options;
        if(options?.indexOf(currentQuestion) > -1 ) {
          this.getQuestion(element.sys_id);
        }else {
          this.showPrevious = false;
        }
    });
  }

  getQuestion(questionId:string) : void {
    const temp:any  = this.tempJson.data; 
    console.log(temp)
    this.question =  { "sys_id": questionId , "label" : temp[questionId].label, "childs": (temp[questionId].options ?  this.getChilds(temp[questionId].options) : [] ) }
    console.log(this.question);
  }


}
