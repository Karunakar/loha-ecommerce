import { Component } from '@angular/core';

import * as contactData from '../../../assets/address.json';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent {

  contact:any;

  constructor() {
    const contact:any = contactData;
    this.contact = contact.default;
  }

}
