import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.sass']
})
export class SocialComponent {

  constructor() {
    this.textColor = 'text-gray-900'
  }

  @Input()
  textColor: string

}
