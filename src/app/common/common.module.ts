import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialComponent } from './social/social.component';
import { ContactComponent } from './contact/contact.component';
import { RouterModule, Routes } from '@angular/router';
import { GoogleMapsModule } from '@angular/google-maps';
import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';


@NgModule({
  declarations: [
    SocialComponent,
    ContactComponent,
    AboutComponent,
    FaqComponent
  ],
  imports: [
    CommonModule,
    GoogleMapsModule
  ],
  exports: [
    SocialComponent
  ]
})
export class CommonCompModule { }
