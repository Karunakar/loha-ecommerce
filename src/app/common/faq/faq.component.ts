import { Component } from '@angular/core';

import * as faqJson from "../../../assets/questions.json";

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.sass']
})
export class FaqComponent {

  faqList:any;

  constructor() {
    const temp:any  = faqJson;
    this.faqList = temp.default;
    console.log(this.faqList);
  }

}
