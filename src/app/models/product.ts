export class Product {
    private name: string;
    private description: string;
    private price: string;

    constructor(name: string, description:string, price: string) {
        this.name = name;
        this.description = description;
        this.price = price;
    }
}